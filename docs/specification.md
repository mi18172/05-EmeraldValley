## Description of use case `Main Menu` ##

 * __Name__: Main Menu

 * __Actors__: User

 * __Short description__: User starts the application. User choose one of the actions that main menu offers.

 * __Basic flow__:
    1. Player starts application, finishes or quits current Gameplay,
returns from one of following: New Game, Continue, Options.
    2. Application show Main menu, which has following buttons: New Game, Continue, Options, Quit.
    3. User clicks on one of the buttons:
        * 3.1 If New Game is clicked, then New Game use case precides.
        * 3.2 If Continue is clicked, then Continue use case precides.
        * 3.3 If Options is clicked, then Options use case precides.
        * 3.4 If Quit is clicked, then Application terminates.

* __Alternative flow__:
    1. If Player forcefully quits Application, Application just terminates.

* __Preconditions__: Application is closed, or in one of the
following states: Gameplay, New Game, Load Game, Settings.

* __Postconditions__: /

* __Sequence Diagram__: 

![Main Menu Sequence Diagram](sequence_diagram_mainmenu.png) 


## Description of use case `Continue` ##

**Name**: `Continue`

**Short description**: User chooses whether he wants to continue one of the previously unfinished games.

**Actors**: User

**Precondition**: Application is running and one of the previous games is unfinished.

**Postconditions**: /

**Event Flow**:

1. User selects button `Continue` from the main menu.

2. Application lists all saved games.
  - 2.1. If the list is empty, player goes back to the main menu.
  - 2.2. User selects wanted game and goes to the step 3. 

3. Application reads file which corresponds to the selected game.
  - 3.1. If the reading fails, player goes to main menu

4. Application starts the game from where the user left off.

**Alternative flow**: /

**Subflow**: /

**Special condition**: /

**Additional information**: /

**Sequence diagram**:

![Sequence diagram Continue](sequence_diagram_continue.png)


## Description of use case `New game` ##

**Name**: `New game`

**Short description**: User, from the main menu, selects 'New game', prompting a new window. This window provides both the access to the configuration of the next game, as well as the option to start it.

**Actors**: User

**Precondition**: Application is in the 'Main menu' state.

**Postconditions**: /

**Event Flow**:

1. User selects the 'New game' button.

2. The 'New game' window appears. From this window:

    * 2.1 User selects total number of players from a drop down menu.
      * 2.1.1 Changes immediately reflect on the number of inputs for the step 2.2.
      * 2.1.2 If the number of players has been increased, disable 2.5 (starting the game) again.

    * 2.2 User inputs the names of the players who will participate in the game into the appropriate input fields.
      * 2.2.1 If all the names have been inputted correctly, enable step 2.5 (starting the game).
      * 2.2.2 If the names have not been inputted correctly, alert the User.

    * 2.3 User chooses the difficulty of the game from a drop down menu.
    * 2.4 User sets the timer duration for the next game by dragging the horizontal scroll bar.
    * 2.5 If the User selects the 'Start game' button, proceed to 'Gameplay'. Disabled at the start.
    * 2.6 If the User selects the 'Back' button, close the 'New game' window, returning them to the main menu.

**Sequence diagram**:

![Sequence diagram New game](sequence_diagram_newgame.png)


## Description of use case `Options` ##

**Name**: `Options`

**Short description**: User selects Options from Main Menu or Pause, new window opens where he can change game options.

**Actors**: User

**Precondition**: App is in Main Menu state or App is in Pause state.

**Postconditions**: Game options are saved.

**Event Flow**:

1. User selects Options button
2. App shows Options window
3. If user selects Music toggle button, music starts or stops based on previous state:
    - 3.1. If toggle was ON now is OFF and music stops
    - 3.2. If toggle was OFF now is ON and music starts
4. If user selects Sound toggle button, sound effects are enabled or disabled based on previous state:
    - 4.1. If toggle was ON now is OFF and sound effects are disabled
    - 4.2. If toggle was OFF now is ON and sound effects are enabled
5. If user selects Back button he is returned to previous state

**Sequence diagram**:

![Sequence diagram Options](sequence_diagram_options.png)

